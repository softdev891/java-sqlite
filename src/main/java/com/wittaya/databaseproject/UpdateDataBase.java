/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author AdMiN
 */
public class UpdateDataBase {
     public static void main(String[] args) {
        //connectDB
        Connection conn = null;
        String url = "jdbc:sqlite:DcoffeeTest.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to Sqlite has been establish");
        } catch (SQLException ex) {
            System.out.println(ex.getNextException());
            return;
        }

        //UPDATE
        String sql = "UPDATE category SET category_name=? WHERE category_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "My Coffee");
            stmt.setInt(2, 1);
            
            int status  = stmt.executeUpdate());
           // ResultSet key = stmt.getGeneratedKeys();
            //key.next();
            //System.out.println(" " + key.getInt(1));

           
        } catch (SQLException ex) {
            System.out.println(ex.getNextException());

        }

        //CloseDB
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getNextException());
            }
        }
    }
}
